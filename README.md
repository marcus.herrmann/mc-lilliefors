# *M*<sub>c</sub><sup>Lilliefors</sup>

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.4162496.svg)](https://doi.org/10.5281/zenodo.4162496)

This repository is associated with the publication:
> Herrmann, M. and W. Marzocchi (2021). Inconsistencies and Lurking Pitfalls in the Magnitude–Frequency Distribution of High-Resolution Earthquake Catalogs. *Seismological Research Letters 92*(2A). doi: [10.1785/0220200337](https://doi.org/10.1785/0220200337)

in which we explore high-resolution earthquake catalogs and analyze whether their magnitude distribution complies with the exponential-like Gutenberg–Richter (GR) relation.
(Spoiler: those catalogs do not preserve the exponential-like magnitude distribution, which characterizes ordinary catalogs, toward low magnitudes.)

Specificially, we estimate a lower magnitude cutoff, or completeness magnitude, *M*<sub>c</sub>, that complies with the exponential-like distribution of the Gutenberg–Richter relation.
Note that an exponential-like distribution above *M*<sub>c</sub> is a necessary and sufficient condition to calculate the *b*-value of the Gutenberg–Richter relation (otherwise the physical meaning of the *b*-value becomes questionable).
We use the [Lilliefors test](https://en.wikipedia.org/wiki/Lilliefors_test) [*Lilliefors* 1969][<sup>1</sup>](#ref1) as a statistical goodness-of-fit test with the exponential distribution to determine the lowest magnitude cutoff above which the magnitude is exponentially distributed, which we call *M*<sub>c</sub><sup>Lilliefors</sup>. In other words, above *M*<sub>c</sub><sup>Lilliefors</sup>, the magnitude–frequency distribution is consistent with the exponential-like Gutenberg–Richter relation.

The Jupyter notebook [`demo-Lilliefors-test.ipynb`](demo-Lilliefors-test.ipynb) (can also be viewed at [nbviewer.jupyter.org](https://nbviewer.jupyter.org/urls/gitlab.com/marcus.herrmann/mc-lilliefors/-/raw/master/demo-Mc-Lilliefors.ipynb) to render the interactive plot) demonstrates our method for one example catalog by making use of the *Python* code in [`mc_lilliefors.py`](mc_lilliefors.py) (the class <tt>McLilliefors</tt>).


Please find more information, applications, and findings in our paper.

<br>

If you are interested in a subsequent <i>b</i>-value estimation, have a look at the [`FMD` class here](https://gitlab.seismo.ethz.ch/microEQ/TM/blob/master/TM/analysis.py#L910), which supports this. Since the `McLilliefors` class is related to it, both are compatible and could be merged (then, only `FMD._calc_Mc()` needs to be modified to support the 'Lilliefors' method).

---

<a id="ref1">*Footnote* [1]</a>
> Lilliefors, H. W. (1969). On the Kolmogorov-Smirnov Test for the Exponential Distribution with Mean Unknown. *Journal of the American Statistical Association, 64*(325), 387–389. doi: [10.2307/2283748](https://doi.org/10.2307/2283748)

&ensp;&ensp;The Lilliefors test is implemented in <tt>statsmodels</tt> ([www.statsmodels.org](https://www.statsmodels.org)):

> Seabold, S. and J. Perktold (2010). [statsmodels: Econometric and statistical modeling with python](http://conference.scipy.org/proceedings/scipy2010/pdfs/seabold.pdf). *Proceedings of the 9th Python in Science Conference*.

<br>

---

Copyright © 2020 Marcus Herrmann · Warner Marzocchi — Università degli Studi di Napoli 'Federico II'

Licensed under the [European Union Public Licence](https://joinup.ec.europa.eu/collection/eupl) ([EUPL-1.2-or-later](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)), the first European Free/Open Source Software (F/OSS) license. It is available in all official languages of the EU.
